from random import randint as random

from .physicalobject import PhysicalObject
from .resources import *

itemscale = 1.0 / 80.0
itemsize = 800.0 * itemscale


class Apple(PhysicalObject):

	def __init__(self, snake_obj, screen_width, screen_height):

		self.new_x, self.new_y = 0, 0

		self.snake_obj = snake_obj
		self.screen_width = screen_width
		self.screen_height = screen_height

		# Initialize the Thing
		super().__init__(img=center_image(pyglet.resource.image("apple.png")),
						 x=0, y=self.new_y * itemsize
						 )
		self.scale = itemscale

		self.newpos()
		self.x = self.new_x * itemsize
		self.y = self.new_y * itemsize

	def getSnekPos(self):
		snekpos = [dict(x=self.snake_obj.head.si_x, y=self.snake_obj.head.si_y)]
		for s in self.snake_obj.body:
			snekpos.append(dict(x=s.si_x, y=s.si_y))

		return snekpos

	def isoverlap(self):
		snekpos = self.getSnekPos()
		for pos in snekpos:
			if self.new_x == pos["x"] and self.new_y == pos["y"]:
				return True
		return False

	def newpos(self, *args):
		if len(args) > 0:
			self.snake_obj = args[0]
		self.new_x, self.new_y = random(1, (self.screen_width / itemsize) - 1), random(1, (
					self.screen_height / itemsize) - 1)
		while self.isoverlap():
			self.new_x, self.new_y = random(1, (self.screen_width / itemsize) - 1), random(1, (
					self.screen_height / itemsize) - 1)
		if len(args) > 0:
			self.x = self.new_x * itemsize
			self.y = self.new_y * itemsize
			self.snake_obj.findpath(self.new_x, self.new_y)
