import pyglet
import hook

from .game_screen import game_screen
from .loading_screen import loading_screen

app = pyglet.app
event_loop = app.EventLoop()

loadscrn = loading_screen()
gamescrn = None


def update(dt):
	if gamescrn is not None and gamescrn.visible:
		gamescrn.update(dt)
	# hook.Run("update", dt=dt)


def start_game():
	global gamescrn
	gamescrn = game_screen()
	gamescrn.activate()
	gamescrn.set_visible(True)
	loadscrn.close()


@loadscrn.event
def on_key_press(symbol, modifiers):
	start_game()


pyglet.clock.schedule_interval(update, 1 / 60)
