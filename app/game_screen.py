from pyglet.window import key
import math

from . import *
from .apple import Apple
from .snake import Snake

itemscale = 1.0 / 80.0
itemsize = 800.0 * itemscale


class game_screen(pyglet.window.Window):
	__name__ = "game_screen"

	def __init__(self):
		super(game_screen, self).__init__(
				width=400,
				height=200,
				caption="Snake Game",
				visible=False
		)

		self.clk = pyglet.clock.get_default()

		self._update_rate = 1 / 60
		self.update_rate_counter = 0

		self._score = 0

		self.saidded = False

		self.labels = dict()
		self.load_labels()
		self.afterdeathdelay = 2
		self.afterdeathdelaycounter = 0

		# Position Table will have (screen_width / itemsize) columns and (screen_height / itemsize) rows

		self.maze_height = self.height / itemsize
		self.maze_width = self.width / itemsize

		print("height: {}".format(self.maze_height))
		print("width: {}".format(self.maze_width))

		# Maze is 2d-Numpy Array indexed as maze[x][y] for (x, y) coordinates
		# Empty Space will be represented by 1
		# Apple will be represented as an empty space
		# Snake Head will be represented by 0


		self.apple = None

		self.snek = Snake(self.width, self.height)

		self.apple = Apple(self.snek, self.width, self.height)

		self.snek.findpath(self.apple.si_x, self.apple.si_y)

		self.update()
		self.update_score()

	@property
	def walkable_area(self):
		return (self.maze_height * self.maze_width) - (self.score + 3)

	def Getmaze(self, x, y):
		if x < 0 or x > self.width / itemsize or y < 0 or y > self.height / itemsize:
			raise Exception("Coordinates out of range")
		elif self.snek.head.si_x == x and self.snek.head.si_y == y:
			return "S"
		elif self.apple.si_x == x and self.apple.si_y == y:
			return "A"
		elif True in (True for s in self.snek.body if s.si_x == x and s.si_y == y):
			return "S"
		else:
			return ""

	@property
	def update_rate(self):
		proxy = game_screen.IProxy(self._update_rate)
		proxy.parent = self
		return proxy

	@update_rate.setter
	def update_rate(self, value):
		self._update_rate = value
		self.labels["speed_label"].text = "Speed: {}".format(self.update_rate)
		if self.clk._schedule_interval_items[0].func.__name__ == "update":
			self.clk._schedule_interval_items[0].interval = self.update_rate

	@staticmethod
	class IProxy(float, object):

		def __iadd__(self, value):
			new_denom = (math.trunc(1 / self.parent.update_rate) + value)
			# We don't want our update rate to be undefined or negative
			if new_denom <= 0 or new_denom > 60:
				return self.parent.update_rate
			self.parent.update_rate = (1 / new_denom)
			return self.parent.update_rate

		def __isub__(self, value):
			new_denom = (math.trunc(1 / self.parent.update_rate) - value)
			# We don't want our update rate to be undefined or negative
			if new_denom <= 0 or new_denom > 60:
				return self.parent.update_rate
			self.parent.update_rate = (1 / new_denom)
			return self.parent.update_rate

		def __repr__(self):
			return "{} units / sec".format(math.trunc(1 / self.parent.update_rate))

	def load_labels(self):
		if len(self.labels) > 0: return;
		self.labels = {
			"score_label": pyglet.text.Label("Score: {}".format(self.score),
											 font_name="Times New Roman",
											 font_size=10,
											 x=10, y=self.height - 20,
											 anchor_x="left", anchor_y="center"
											 ),
			# "speed_label": pyglet.text.Label("Speed: {}".format(self.update_rate),
			# 								 font_name="Times New Roman",
			# 								 font_size=10,
			# 								 x=10, y=self.height - 40,
			# 								 anchor_x="left", anchor_y="center"
			# 								 )
		}

	def on_draw(self):
		self.clear()

		for l in self.labels.values():
			l.draw()

		self.snek.draw()

		self.apple.draw()

	# @hook.Add("update", nsoc=True)
	def update(self, dt=None):
		if not self.snek.dead and len(self.snek.path) > 0:
			self.snek.check_collides()
			if self.snek.path[0] == "up":
				self.snek.up()
			elif self.snek.path[0] == "down":
				self.snek.down()
			elif self.snek.path[0] == "left":
				self.snek.left()
			elif self.snek.path[0] == "right":
				self.snek.right()
			self.snek.path.pop(0)
			self.snek.update(dt=dt)
			if self.snek.head.collides_with(self.apple):
				print("Scored!")
				self.snek.add_segments(1)
				self.score = len(self.snek.body) - 2
				self.apple.newpos(self.snek)
			self.update_rate_counter = 0
		elif not self.snek.dead and len(self.snek.path) == 0:
			self.snek.findpath(self.apple.si_x, self.apple.si_y)
		elif self.snek.dead:
			if not self.saidded:
				print("Died! @ ({},{}) facing: {}".format(self.snek.head.si_x, self.snek.head.si_y, self.snek.head.direction))
				self.saidded = True

	@hook.Add("score_changed", nsoc=True)
	def update_score(self):
		self.labels["score_label"].text = "Score: {}".format(self._score)

	def on_key_press(self, symbol, modifiers):
		# standard fucking controls for the actual game
		# if symbol == key.UP:
		# 	self.snek.up()
		# elif symbol == key.DOWN:
		# 	self.snek.down()
		# elif symbol == key.LEFT:
		# 	self.snek.left()
		# elif symbol == key.RIGHT:
		# 	self.snek.right()
		# el
		if symbol == key.SPACE:  # Revive
			self.snek.kill()
			self.score = 0
			self.snek = None
			self.snek = Snake(self.width, self.height)
			self.saidded = False
		elif symbol == key.ESCAPE:
			self.close()
		elif symbol == key.PAGEUP:
			self.update_rate += 1
		elif symbol == key.PAGEDOWN:
			self.update_rate -= 1

	@property
	def score(self):
		return self._score

	@score.setter
	def score(self, value):
		self._score = value
		hook.Run("score_changed")
