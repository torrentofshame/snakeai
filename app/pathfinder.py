class Node:

	def __init__(self, parent=None, position=None, direction=None):
		self.parent = parent
		self.position = position
		self.__direction = direction

		self.g = 0
		self.h = 0
		self.f = 0

	def __eq__(self, other):
		return self.position == other.position

	def __lt__(self, other):
		if self.f != other.f:
			return self.f < other.f
		else:
			return self.h < other.h

	def __gt__(self, other):
		if self.f != other.f:
			return self.f > other.f
		else:
			return self.h > other.h

	@property
	def direction(self):
		if self.parent is None:
			return self.__direction
		elif self.position[0] == self.parent.position[0] and self.position[1] > self.parent.position[1]:
			return "up"
		elif self.position[0] == self.parent.position[0] and self.position[1] < self.parent.position[1]:
			return "down"
		elif self.position[0] < self.parent.position[0] and self.position[1] == self.parent.position[1]:
			return "left"
		elif self.position[0] > self.parent.position[0] and self.position[1] == self.parent.position[1]:
			return "right"
		else:
			if self.position[0] < self.parent.position[0] and self.position[1] > self.parent.position[1]:
				return "up left"
			elif self.position[0] > self.parent.position[0] and self.position[1] > self.parent.position[1]:
				return "up right"
			elif self.position[0] < self.parent.position[0] and self.position[1] < self.parent.position[1]:
				return "down left"
			elif self.position[0] > self.parent.position[0] and self.position[1] < self.parent.position[1]:
				return "down right"


	@direction.setter
	def direction(self, value):
		self.__direction = value

	@property
	def dirinverse(self):
		if self.direction == "up":
			return "down"
		elif self.direction == "down":
			return "up"
		elif self.direction == "left":
			return "right"
		elif self.direction == "right":
			return "left"
		else:
			return None
