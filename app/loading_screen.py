import pyglet
import hook


class loading_screen(pyglet.window.Window):
	__name__ = "loading_screen"

	def __init__(self):
		super(loading_screen, self).__init__(
				width=400,
				height=200,
				caption="Snake Game"
		)

		self.score = None

		self.labels = {
			"title_label": pyglet.text.Label("Snake Game",
											 font_name='Times New Roman',
											 font_size=45,
											 x=self.width // 2, y=self.height // 2,
											 anchor_x='center', anchor_y='center'
											 ),
			# "instruction_label": pyglet.text.Label("Some Fucking Great Instructions go Here",
			# 									   font_name='Times New Roman',
			# 									   font_size=15,
			# 									   x=self.width // 2, y=self.height // 3,
			# 									   anchor_x='center', anchor_y='center'
			# 									   ),
			"press_anykey_label": pyglet.text.Label("Press the Any Key to Start",
													font_name='Times New Roman',
													font_size=10,
													color=(0, 255, 0, 255),
													x=self.width // 2, y=self.height // 4,
													anchor_x='center', anchor_y='center',
													)
		}

	def on_draw(self):
		self.clear()

		for l in self.labels.values():
			l.draw()
