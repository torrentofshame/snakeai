import pyglet, math

# from . import *

itemscale = 1.0 / 80.0
itemsize = 800.0 * itemscale


class PhysicalObject(pyglet.sprite.Sprite):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.prevdir = None
		self._direction = None
		self.dead = False

	@property
	def direction(self):
		return self._direction

	@direction.setter
	def direction(self, value):
		self.prevdir = self._direction
		self._direction = value

	def update(self, dt):
		self.wall_whack()

	def wall_whack(self, x, y):
		min_x = itemsize
		min_y = itemsize
		max_x = 790 if self.screen_width is None else self.screen_width - itemsize
		max_y = 390 if self.screen_height is None else self.screen_height - itemsize
		if x < min_x or x > max_x or y < min_y or y > max_y:
			self.dead = True

	@staticmethod
	def distance(point_1=(0, 0), point_2=(0, 0)):
		return math.sqrt(
				(point_1[0] - point_2[0]) ** 2 +
				(point_1[1] - point_2[1]) ** 2)

	@property
	def si_x(self):
		return self.x/itemsize

	@property
	def si_y(self):
		return self.y/itemsize
