from concurrent.futures import ThreadPoolExecutor, as_completed
from .pathfinder import Node
from .resources import *
from .physicalobject import PhysicalObject
from .heap import Heap
from .apple import Apple
import numpy as np
from scipy.ndimage import measurements
import math
import pyglet

itemscale = 1.0 / 80.0
itemsize = 800.0 * itemscale


class Snake:
	maxspeed = 50.0
	D = 1

	def __init__(self, screen_width, screen_height):
		self.screen_width, self.screen_height = screen_width, screen_height
		self.head = Head(screen_width, screen_height)
		self.body = [Segment(self.head, 0)]
		self.body.append(Segment(self.body[0], 1))
		self.head.velocity = self.maxspeed, 0.0
		self.dead = self.head.dead
		self.path = []
		self.count = 0
		for win in pyglet.app.windows:
			if win.__name__ == "game_screen":
				self.scrn = win

	def kill(self):
		self.head.delete()
		for s in self.body:
			s.delete()

	def draw(self):

		# draw head
		self.head.draw()
		# draw body
		for s in self.body:
			s.draw()

	def add_segments(self, count=0):
		if count == 0: return;
		self.body.append(Segment(self.body[-1], len(self.body)))

	def check_collides(self):
		for s in self.body:
			if self.head.collides_with(s):
				self.head.handle_collision_with(s)

	def update(self, dt=None):
		self.head.update(dt=dt)
		for s in self.body:
			s.update(dt=dt)
		self.dead = self.head.dead

	def up(self):
		if self.head.direction == "down": return;
		self.head.direction = "up"
		self.head.velocity = 0.0, self.maxspeed

	def down(self):
		if self.head.direction == "up": return;
		self.head.direction = "down"
		self.head.velocity = 0.0, -1 * self.maxspeed

	def left(self):
		if self.head.direction == "right": return;
		self.head.direction = "left"
		self.head.velocity = -1 * self.maxspeed, 0.0

	def right(self):
		if self.head.direction == "left": return;
		self.head.direction = "right"
		self.head.velocity = self.maxspeed, 0.0

	@staticmethod
	def is_position_valid(position):
		if 0 <= position[0] <= 40 and 0 <= position[1] <= 20:
			return True
		else:
			return False

	@staticmethod
	def predict_segment_positions(node, numofsegments):
		segments = []
		current = node
		for i in range(numofsegments):
			if current.parent is not None and Snake.is_position_valid(current.parent.position):
				segments.append(current.parent.position)
				current = current.parent
			else:
				break
		return segments

	@staticmethod
	def heuristic(nodepos, endpos):
		dx = abs(nodepos[0] - endpos[0])
		dy = abs(nodepos[1] - nodepos[1])
		return Snake.D * (dx + dy)

	@staticmethod
	def find_children(current_node, new_position, scrn, closed_list, open_list, end_node):
		# Get node position
		node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

		# Make sure within range
		if node_position[0] > ((scrn.width // itemsize) - 1) or node_position[0] < 1 or node_position[
			1] > ((scrn.height // itemsize) - 1) or node_position[1] < 1:
			return None

		# Make sure walkable terrain
		if scrn.Getmaze(int(node_position[0]), int(node_position[1])) == "S":  # If space is non-walkable terrain
			return None

		# Create new child node
		child = Node(current_node, node_position)

		# Make sure we don't turn 180 in one move
		if current_node.dirinverse == child.direction:
			return None
		elif child.direction not in ["up", "down", "left", "right"]:
			if current_node.direction in ("up", "down"):
				child0 = Node(current_node, (child.position[0], 0))
				child = (Node(child0, (0, child.position[1])), child0)
			elif current_node.direction in ("left", "right"):
				child0 = Node(current_node, (0, child.position[0]))
				child = (Node(child0, (child.position[1], 0)), child0)

		# Now Process the Child -------------------------------------------------------------
		if not isinstance(child, tuple):

			# Check against 80% rule

			# Initialize Maze Array
			maze = np.array([[1 for r in range(int(scrn.maze_height))] for c in range(int(scrn.maze_width))])  # Initialized with all 1s
			# Populate Maze Array with snake positions
			# maze[int(child.position[0])][int(child.position[1])] = 0
			predictedpos = Snake.predict_segment_positions(child, scrn.score + 3)
			for x, y in predictedpos:
				maze[int(x-1)][int(y-1)] = 0

			# Label Maze
			lw, num = measurements.label(maze)

			# Calculate areas
			area = measurements.sum(maze, lw, index=np.arange(lw.max() + 1))
			areaimg = area[lw]

			# Total walkable terrian
			walkabletotal = sum([i for i in area if i != 0])

			future_positions = [
				(-1, 0),
				(1, 0),
				(0, 1),
				(0, -1)
			]

			reachableareas = {}
			for dx, dy in future_positions:
				x, y = child.position[0] + dx, child.position[1] + dy
				if x < 0 or x >= scrn.maze_width or y < 0 or y >= scrn.maze_height: continue;
				if lw[int(x)][int(y)] != 0 and str(lw[int(x)][int(y)]) not in reachableareas.keys():
					reachableareas[str(lw[int(x)][int(y)])] = areaimg[int(x)][int(y)]

			reachableareatotal = sum(list(reachableareas.values()))
			reachableareapercent = 100 * reachableareatotal // walkabletotal
			if reachableareapercent < 80:  # If node cannot reach at least 80% of all walkable area, don't use it
				return None

			# End 80% rule

			for closed_child in closed_list:
				if child == closed_child:
					return None

			# Create the f, g, and h values
			child.g = current_node.g + 1
			child.h = Snake.heuristic(child.position, end_node.position)
			child.f = child.g + child.h

			# Child is already in the open list
			for open_node in open_list:
				if child == open_node and child.g > open_node.g:
					return None
		elif child is not None and isinstance(child, tuple):
			for i in [0, 1]:

				# Check against 80% rule

				# Initialize Maze Array
				maze = np.array([[1 for r in range(int(scrn.maze_height))] for c in range(int(scrn.maze_width))])  # Initialized with all 1s
				# Populate Maze Array with snake positions
				# maze[int(child[i].position[0])][int(child[i].position[1])] = 0
				predictedpos = Snake.predict_segment_positions(child[i], scrn.score + 3)
				for x, y in predictedpos:
					maze[int(x-1)][int(y-1)] = 0

				# Label Maze
				lw, num = measurements.label(maze)

				# Calculate areas
				area = measurements.sum(maze, lw, index=np.arange(lw.max() + 1))
				areaimg = area[lw]

				# Total walkable terrian
				walkabletotal = sum([i for i in area if i != 0])

				future_positions = [
					(-1, 0),
					(1, 0),
					(0, 1),
					(0, -1)
				]

				reachableareas = {}
				for dx, dy in future_positions:
					x, y = child[i].position[0] + dx, child[i].position[1] + dy
					if x < 0 or x >= scrn.maze_width or y < 0 or y >= scrn.maze_height: continue;
					if lw[int(x)][int(y)] != 0 and str(lw[int(x)][int(y)]) not in reachableareas.keys():
						reachableareas[str(lw[int(x)][int(y)])] = areaimg[int(x)][int(y)]

				reachableareatotal = sum(list(reachableareas.values()))
				reachableareapercent = 100 * reachableareatotal // walkabletotal
				if reachableareapercent < 80:  # If node cannot reach at least 80% of all walkable area, don't use it
					if i == 0:
						return None
					elif i == 1:
						return child[0]

				# End 80% rule

				for closed_child in closed_list:
					if child[i] == closed_child:
						if i == 0:
							return None  # If the first part shouldn't be passed, none shall
						elif i == 1:
							return child[0]  # If the second part shouldn't be passed, pass only the first one

				# Create the f, g, and h values
				child[i].g = (current_node.g + 1) if i == 0 else (current_node.g + 2)
				child[i].h = Snake.heuristic(child[i].position, end_node.position)
				child[i].f = child[i].g + child[i].h

				# Child is already in the open list
				for open_node in open_list:
					if child[i] == open_node and child[i].g > open_node.g:
						if i == 0:
							return None  # If the first part shouldn't be passed, none shall
						elif i == 1:
							return child[0]  # If the second part shouldn't be passed, pass only the first one

		return child

	def astar(self, apple_x, apple_y):
		"""Returns a list of tuples as a path from the given start to the given end in the given maze"""
		start = (self.head.si_x, self.head.si_y)
		end = (apple_x, apple_y)

		# Create start and end node
		start_node = Node(None, start, self.head.direction)
		start_node.g = start_node.h = start_node.f = 0
		end_node = Node(None, end)
		end_node.g = end_node.h = end_node.f = 0

		print("Currently Facing {} @ ({}, {})".format(start_node.direction, start[0], start[1]))
		print("Apple is to the {} @ ({}, {})".format(Node(start_node, end).direction, end[0], end[1]))

		# Initialize both open and closed list
		open_list = Heap()
		closed_list = []

		# Add the start node
		open_list.append(start_node)

		# Loop until you find the end
		while len(open_list) > 0:

			# Get the current node
			current_node = open_list.min()
			closed_list.append(current_node)

			# Found the goal
			if current_node == end_node:
				path = []
				current = current_node
				while current is not None:
					path.append(current.direction)
					current = current.parent
				return path[::-1]  # Return reversed path

			# Generate children
			children = []

			avail_squares = []
			if current_node.direction == "up":
				avail_squares = [
					(0, 1),  # Up
					(-1, 0),  # Left
					(1, 0),  # Right
					(-1, 1),  # UpLeft
					(1, 1),  # UpRight
					(-1, -1),  # DownLeft
					(1, -1)  # DownRight
				]
			elif current_node.direction == "down":
				avail_squares = [
					(0, -1),  # Down
					(-1, 0),  # Left
					(1, 0),  # Right
					(-1, 1),  # UpLeft
					(1, 1),  # UpRight
					(-1, -1),  # DownLeft
					(1, -1)  # DownRight
				]
			elif current_node.direction == "left":
				avail_squares = [
					(0, 1),  # Up
					(0, -1),  # Down
					(-1, 0),  # Left
					(-1, 1),  # UpLeft
					(1, 1),  # UpRight
					(-1, -1),  # DownLeft
					(1, -1)  # DownRight
				]
			elif current_node.direction == "right":
				avail_squares = [
					(0, 1),  # Up
					(0, -1),  # Down
					(1, 0),  # Right
					(-1, 1),  # UpLeft
					(1, 1),  # UpRight
					(-1, -1),  # DownLeft
					(1, -1)  # DownRight
				]

			processes = []
			with ThreadPoolExecutor(max_workers=7) as executor:
				for new_position in avail_squares:  # Adjacent squares
					processes.append(executor.submit(
							self.find_children,
							current_node=current_node,
							new_position=new_position,
							scrn=self.scrn,
							closed_list=closed_list,
							open_list=open_list,
							end_node=end_node))
			for task in as_completed(processes):
				result = task.result()
				if result is not None and not isinstance(result, tuple):
					open_list.append(result)
				elif result is not None and isinstance(result, tuple):
					open_list.append(result[0])
					open_list.append(result[1])

	def findpath(self, apple_x, apple_y):
		new_path = self.astar(apple_x, apple_y)[1:]
		if len(new_path) > 0:
			self.path = new_path


class Head(PhysicalObject):

	def __init__(self, screen_width, screen_height):
		self.screen_width = screen_width
		self.screen_height = screen_height
		super().__init__(img=center_image(pyglet.resource.image("segment.png")), x=self.screen_width / 2,
						 y=self.screen_height / 2)
		self.dead = False
		self.scored = False
		self.scale = itemscale
		self._direction = "right"
		self.prevdir = "right"
		self.whatis = "head"

	def update(self, dt):
		new_x, new_y = self.x, self.y
		if self.direction == "up":
			new_y += itemsize
		elif self.direction == "down":
			new_y -= itemsize
		elif self.direction == "left":
			new_x -= itemsize
		elif self.direction == "right":
			new_x += itemsize
		self.wall_whack(new_x, new_y)
		self.x, self.y = new_x, new_y

	def collides_with(self, other_object):
		iscollided = other_object.position == self.position
		return iscollided

	def handle_collision_with(self, other_obj):
		if isinstance(other_obj, Segment):
			print(other_obj)
			self.dead = True


class Segment(PhysicalObject):
	maxspeed = 50.0

	def __init__(self, following, idnum):
		self.following = following
		self.idnum = idnum
		super().__init__(img=center_image(pyglet.resource.image("segment.png")),
						 x=self.following.x - itemsize,
						 y=self.following.y
						 )
		self.scale = itemscale
		self._direction = "right"
		self.prevdir = "right"
		self.whatis = "body"

	def update(self, dt):
		if self.direction == "up":
			self.y += itemsize
		elif self.direction == "down":
			self.y -= itemsize
		elif self.direction == "left":
			self.x -= itemsize
		elif self.direction == "right":
			self.x += itemsize
		if self.following.whatis == "head":
			self.direction = self.following.direction
		else:
			self.direction = self.following.prevdir

	def __repr__(self):
		return "Segment {}".format(self.idnum)
