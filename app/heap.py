class Heap(object):

	def __init__(self):
		self.heap_list = [None]
		self.count = 0

	def __len__(self):
		return self.count

	def __iter__(self):
		return iter(self.heap_list[1:])

	def parent_index(self, index):
		return index // 2

	def left_child_index(self, index):
		return index * 2

	def right_child_index(self, index):
		return index * 2 + 1

	def child_present(self, index):
		return self.left_child_index(index) <= self.count

	def min(self):
		if self.count == 0:
			return None

		min = self.heap_list[1]
		self.heap_list[1] = self.heap_list[self.count]
		self.count -= 1
		self.heap_list.pop()
		self.heapify_down()
		return min

	def append(self, element):
		self.count += 1
		self.heap_list.append(element)
		self.heapify_up()

	def get_smaller_child_index(self, index):
		if self.right_child_index(index) > self.count:
			return self.left_child_index(index)
		else:
			left_child = self.heap_list[self.left_child_index(index)]
			right_child = self.heap_list[self.right_child_index(index)]
			return self.left_child_index(index) if left_child < right_child else self.right_child_index(index)

	def heapify_up(self):
		index = self.count
		while self.parent_index(index) > 0:
			if self.heap_list[self.parent_index(index)] > self.heap_list[index]:
				tmp = self.heap_list[self.parent_index(index)]
				self.heap_list[self.parent_index(index)] = self.heap_list[index]
				self.heap_list[index] = tmp
			index = self.parent_index(index)

	def heapify_down(self):
		index = 1
		while self.child_present(index):
			smaller_child_index = self.get_smaller_child_index(index)
			if self.heap_list[index] > self.heap_list[smaller_child_index]:
				tmp = self.heap_list[smaller_child_index]
				self.heap_list[smaller_child_index] = self.heap_list[index]
				self.heap_list[index] = tmp
			index = smaller_child_index
